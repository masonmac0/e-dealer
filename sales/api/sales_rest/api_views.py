from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from common.json import ModelEncoder
from .models import SalesPerson, SaleRecord, AutomobileVO, Customer
import json


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "id",
        "vin",
        "is_sold",
        "import_href",
    ]

class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "id",
        "name",
        "address",
        "phone_number"
    ]

class SalesPersonEncoder(ModelEncoder):
    model = SalesPerson
    properties = [
        "id",
        "name",
        "employee_number"
    ]

class SaleRecordEncoder(ModelEncoder):
    model = SaleRecord
    properties = [
        "id",
        "sale_price",
        "customer",
        "sales_person",
        "automobile"
    ]
    encoders = {
        "automobile": AutomobileVOEncoder(),
        "customer": CustomerEncoder(),
        "sales_person": SalesPersonEncoder(),
    }



@require_http_methods(["GET", "POST"])
def api_list_sales(request):
    if request.method == "GET":
        sale = SaleRecord.objects.all()
        return JsonResponse(
            {"sale": sale},
            encoder=SaleRecordEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        try:
            automobile = AutomobileVO.objects.get(vin=content["automobile"])
            content["automobile"] = automobile
        except AutomobileVO.DoesNotExist:
            response = JsonResponse(
                {"message": "Car not found"},
            )
            response.status_code = 400
            return response
        try:
            customer = Customer.objects.get(id=content["customer"])
            content["customer"] = customer
        except Customer.DoesNotExist:
            response = JsonResponse(
                {"message": "Customer not found"},
            )
            response.status_code = 400
            return response
        try:
            sales_person = SalesPerson.objects.get(id=content["sales_person"])
            content["sales_person"] = sales_person
        except SalesPerson.DoesNotExist:
            response = JsonResponse(
                {"message": "Sales Person not found"},
            )
            response.status_code = 400
            return response
        sale_record = SaleRecord.objects.create(**content)
        automobile.is_sold = True
        automobile.save()
        return JsonResponse(
            sale_record,
            encoder=SaleRecordEncoder,
            safe=False
        )




@require_http_methods(["GET", "POST"])
def api_list_salesperson(request):
    if request.method == "GET":
        sales_person = SalesPerson.objects.all()
        return JsonResponse(
            {"sales_person": sales_person},
            encoder=SalesPersonEncoder,
            safe=False
        )
    else:
        try:
            content = json.loads(request.body)
            sales_person = SalesPerson.objects.create(**content)
            return JsonResponse(
                sales_person,
                encoder=SalesPersonEncoder,
                safe=False
            )
        except Exception as ex:
            response = JsonResponse(
                {"message": "Could not create employee"},
            )
            response.status_code = 400
            return response


@require_http_methods(['GET'])
def api_show_salesperson(request, pk):
    if request.method == 'GET':
        salesperson = SalesPerson.objects.get(id=pk)
        return JsonResponse(
            salesperson,
            encoder=SalesPersonEncoder,
            safe=False,
        )

@require_http_methods(["GET", "POST"])
def api_list_customer(request):
    if request.method == "GET":
        customer = Customer.objects.all()
        return JsonResponse(
            {"customer": customer},
            encoder = CustomerEncoder,
            safe=False
        )
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder = CustomerEncoder,
                safe=False
            )
        except:
            response = JsonResponse(
                {"message": "Could not create customer"}
            )
            response.status_code = 400
            return response

@require_http_methods(["GET"])
def api_automobiles(request):
    if request.method == "GET":
        automobile = AutomobileVO.objects.all()
        return JsonResponse(
            {"autos": automobile},
            encoder=AutomobileVOEncoder,
            safe=False,
        )
