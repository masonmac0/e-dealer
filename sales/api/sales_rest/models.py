from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True, blank=True, null=True)
    vin = models.CharField(max_length=17, unique=True)
    is_sold = models.BooleanField(default=False)

    def __str__(self):
        return self.vin

    def get_api_url(self):
        return reverse("api_automobile", kwargs={"vin": self.vin})


class Customer(models.Model):
    name = models.CharField(max_length=200)
    address = models.TextField(max_length=500, null=True)
    phone_number = models.CharField(max_length=13)

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("api_show_customer", kwargs={"id": self.id})


class SalesPerson(models.Model):
    name = models.CharField(max_length=200)
    employee_number = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("api_show_sales_person", kwargs={"id": self.id})


class SaleRecord(models.Model):
    sale_price = models.IntegerField()
    customer = models.ForeignKey(
        Customer,
        related_name="sale_record",
        on_delete=models.CASCADE
    )
    sales_person = models.ForeignKey(
        SalesPerson,
        related_name="sale_record",
        on_delete=models.CASCADE
    )
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="sale_record",
        on_delete=models.CASCADE
    )

    def __str__(self):
        return f'{self.sales_person} - {self.automobile}'

    def get_api_url(self):
        return reverse("api_list_sale_record", kwargs={"id": self.id})
    