from django.urls import path
from .api_views import (
    api_list_salesperson,
    api_list_customer,
    api_automobiles,
    api_list_sales,
    api_show_salesperson,
)


urlpatterns = [
    path('salesperson/', api_list_salesperson, name='api_create_salesperson'),
    path('customer/', api_list_customer, name='api_create_customer'),
    path('automobilevo/', api_automobiles, name='automobile_list'),
    path('sales/', api_list_sales, name='api_create_sales'),
    path('salesperson/<int:pk>/', api_show_salesperson, name='api_show_salesperson'),
]
