from django.contrib import admin

from .models import AutomobileVO, Customer, SalesPerson, SaleRecord

# Register your models here.
@admin.register(AutomobileVO)
class AutomobileVO(admin.ModelAdmin):
    pass

@admin.register(Customer)
class Customer(admin.ModelAdmin):
    pass

@admin.register(SalesPerson)
class SalesPerson(admin.ModelAdmin):
    pass


@admin.register(SaleRecord)
class SalesRecord(admin.ModelAdmin):
    pass
