import React, {useEffect, useState} from 'react';

function VehicleModelLists() {
    const [models, setVehicleModels] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/models/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            const models = data.models;
            setVehicleModels(models);
        }

    }

    useEffect(() => {
        fetchData();
    }, []);



    function deleteModel(id) {
        fetch(`http://localhost:8100/api/models/${id}`, {
            method: 'DELETE',
        }).then((result) => {
            result.json().then((r) => {
                console.warn(r);
                fetchData();
            })
        })
    }


    if (models === undefined) {
        return null;
    }
    return (
        <div className='row'>
        <div className='col-md-12'>
            <h1>Vehicle Models</h1>
            <table className='table table-striped'>
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Manufacturer</th>
                        <th>Picture</th>
                    </tr>
                </thead>
                <tbody>
                    {models.map(vehicleModel => {
                        return (
                            <tr key={vehicleModel.id}>
                                <td>{vehicleModel.name}</td>
                                <td>{vehicleModel.manufacturer.name}</td>
                                <td><img src={vehicleModel.picture_url} alt="" height="100" /></td>
                                <td><button onClick={() => deleteModel(vehicleModel.id)} type="button" className="btn btn-danger">Delete</button></td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
        </div>
    );


}

export default VehicleModelLists;
