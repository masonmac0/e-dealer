import React, {useState} from 'react';

function SalespersonForm() {
    const [name, setName] = useState('');
    const [employeeNumber, setEmployeeNumber] = useState('');

    const handleNameChange = (e) => {
        const value = e.target.value;
        setName(value);
    }
    const handleEmployeeNumberChange = (e) => {
        const value = e.target.value;
        setEmployeeNumber(value);
    }


    const handleSubmit = async (e) => {
        e.preventDefault();
        const data = {};
        data.name = name;
        data.employee_number = employeeNumber;

        const createEmployeeUrl = 'http://localhost:8090/api/salesperson/';
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        };
        const response = await fetch(createEmployeeUrl, fetchConfig);
        if (response.ok) {
            const newEmployee = await response.json();
            setName('');
            setEmployeeNumber('');
        }
    }

    return (
        <div className="my-5 container">
        <div className="row">
        <div className="col">
          <div className="card shadow">
            <div className="card-body">
              <form onSubmit={handleSubmit} id="hat-form">
                <h1 className="card-title">Add a new employee</h1>
                <p className="mb-3">
                  Please create a new employee.
                </p>
                  <div className="col">
                      <div className="form-floating mb-3">
                        <input onChange={handleNameChange} required placeholder="name" type="text" id="name" name="name" className="form-control" value={name}/>
                        <label htmlFor="style_name">Employee Name</label>
                      </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                        <input onChange={handleEmployeeNumberChange} required placeholder="employee number" type="text" id="employee-number" name="employee-number" className="form-control" value={employeeNumber}/>
                        <label htmlFor="fabric">Employee Number</label>
                    </div>
                  </div>
                  <div className="col">
                    <button type="submit" className="btn btn-primary">Submit</button>
                  </div>
              </form>
              </div>
              </div>
              </div>
              </div>
              </div>

        )
}


export default SalespersonForm;
