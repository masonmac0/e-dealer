import React, {useState, useEffect} from 'react';

function SalesRecordForm(){
    const [autos, setAutomobiles] = useState([]);
    const [customer, setCustomer] = useState([]);
    const [sales_person, setSalesPersons] = useState([]);

    const fetchCustomer = async () => {
        const url = 'http://localhost:8090/api/customer/';
        const response = await fetch(url);
        if (response.ok){
            const data = await response.json();
            setCustomer(data.customer)

            }
        };

        const fetchAutos = async () => {
        const url2 = 'http://localhost:8090/api/automobilevo/';
        const response2 = await fetch(url2);
        if (response2.ok){
            const data2 = await response2.json();
            setAutomobiles(data2.autos)
            }
        };
    const fetchSalesPerson = async () => {
        const url3 = 'http://localhost:8090/api/salesperson/';
        const response3 = await fetch(url3);
        if (response3.ok){
            const data3 = await response3.json();
            setSalesPersons(data3.sales_person)
            }
        };


    const [sale_price, setSalePrice] = useState('');
    const [buyer, setBuyer] = useState('');
    const [salesperson, setSalePerson] = useState('');
    const [vin, setVin] = useState('');

    const handleSalePriceChange = event => {
        const value = event.target.value
        setSalePrice(value)
    }

    const handleBuyerChange = event => {
        const value = event.target.value
        setBuyer(value)
    }
    const handleSalePersonChange = event => {
        const value = event.target.value
        setSalePerson(value)
    }
    const handleVinChange = event => {
        const value = event.target.value
        setVin(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {}

        data.sales_person = parseInt(salesperson)
        data.automobile= vin
        data.customer = parseInt(buyer)
        data.sale_price = parseInt(sale_price);

        const salesUrl = `http://localhost:8090/api/sales/`
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }
        const response = await fetch(salesUrl, fetchConfig);
        if (response.ok) {
            const newSalesRecord = await response.json();
            setSalePrice('')
            setBuyer('')
            setSalePerson('')
            setVin('')
        }
        }

        const filteredAutos = autos.filter(
            (autos) => !autos.is_sold
        );


    useEffect(() => {
        fetchAutos();
        fetchCustomer();
        fetchSalesPerson();
        }, []);


    return (
        <div className="container">
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a new Sales Record</h1>
                <form onSubmit={handleSubmit} id="create-appointment-form">
                <div className="form-floating mb-3">
                    <input onChange={handleSalePriceChange} value={sale_price} placeholder="Price" required type="number" name="sale_price" id="sale_price" className="form-control"></input>
                    <label> Sale Price </label>
                </div>
                <div className="form-floating mb-3">
                    <select onChange={handleBuyerChange} value={buyer} placeholder="Customer" required type="text"  id="buyer" className="form-control">
                    <option value="" >Customer </option>
                    {customer.map(buy=>{
                        return (
                            <option value={buy.id} key={buy.id} >
                                {buy.name}
                            </option>
                        )
                    })}
                    </select>
                </div>
                <select onChange={handleSalePersonChange} value={salesperson} placeholder="Sales Person" type="number"  id="sales_person" className="form-control">
                    <option value ="" > Sales Person</option>
                    {sales_person.map(person=>{
                        return (
                            <option value={person.id} key={person.id} >
                                {person.name}
                            </option>
                        )
                    })}
                    </select>
                <div className="form-floating mb-3">
                    <select onChange={handleVinChange} value={vin} placeholder="Vin" type="number"  id="vin" className="form-control">
                    <option value ="" > VIN</option>
                    {filteredAutos.map(auto=>{
                        return (
                            <option value={auto.vin} key={auto.id} >
                                {auto.vin}
                            </option>
                        )
                    })}
                    </select>
                </div>
                <button className="btn btn-primary">Create</button>
                </form>
            </div>
            </div>
        </div>
        </div>
        );
    }
export default SalesRecordForm
