import React, {useState, useEffect} from 'react';


function SalesList() {
    const [sale, setSales] = useState([]);
    const fetchSales = async () => {
        const response = await fetch('http://localhost:8090/api/sales/');
        const data = await response.json();
        setSales(data.sale);
    }
    useEffect(() => {
        fetchSales();
    }, []);

    return (
        <table className="table table-striped">
        <thead>
            <tr>
                <th>Employee Name</th>
                <th>Employee Number</th>
                <th>Purchaser Name</th>
                <th>VIN</th>
                <th>Price</th>
            </tr>
        </thead>
        <tbody>
            {sale.map(sold => {
                return (
                    <tr key={sold.id}>
                        <td>{sold.sales_person.name}</td>
                        <td>{sold.sales_person.employee_number}</td>
                        <td>{sold.customer.name}</td>
                        <td>{sold.automobile.vin}</td>
                        <td>${sold.sale_price.toLocaleString() }</td>
                    </tr>
                );
            })}
        </tbody>
        </table>
        );
}

export default SalesList;
