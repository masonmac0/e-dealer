import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ManufacturersList from './ManufacturersList'
import ManufacturersForm from './ManufacturersForm'
import AppointmentList from './AppointmentList'
import TechnicianForm from './TechnicianForm'
import AppointmentForm from './AppointmentForm'
import AutomobileList from './AutomobileLists'
import AutomobileForm from './AutomobileForms'
import VehicleModelLists from './VehicleModelLists'
import VehicleModelsForm from './VehicleModelsForm'
import SalespersonForm from './CreateSalesperson'
import NewCustomerForm from './CreateCustomerForm';
import CreateSaleRecord from './CreateSaleRecordForm';
import AppointmentHistory from './AppointmentHistory';
import SalesList from './ListAllSales';
import SalesPersHist from './ListSalesPersHist';



function App(props) {

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/manufacturers">
            <Route path="" element={<ManufacturersList /> } />
            <Route path = "new" element={<ManufacturersForm/>} />
          </Route>
          <Route path ="/models" >
            <Route index element={<VehicleModelLists />} />
            <Route path="new" element={<VehicleModelsForm/>} />
          </Route>
            <Route path="/automobiles">
            <Route path="" element={<AutomobileList /> } />
            <Route path = "new" element={<AutomobileForm/>} />
          </Route>
          <Route path="/technicians">
            <Route path= "" element={<TechnicianForm/>} />
            </Route>
          <Route path="/appointments">
            <Route path= "" element={<AppointmentList />} />
            <Route path = "new" element={<AppointmentForm/>} />
            <Route path = "history" element={<AppointmentHistory/>} />
            </Route>
          <Route path="/salesperson" element={<SalespersonForm />}/>
          <Route path="/customer" element={<NewCustomerForm />}/>
          <Route path="/sales" element={<CreateSaleRecord />}/>
          <Route path="/sales/history" element={<SalesList />}/>
          <Route path="/sales/history/hx" element={<SalesPersHist />}/>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
