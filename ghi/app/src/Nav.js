import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">E-Dealer</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
          <li className="nav-item">
            <NavLink className="nav-link active" aria-current="page" id="new-location-nav" to="/manufacturers">
              Manufacturers
            </NavLink>
            </li>
            <li className="nav-item">
            <NavLink className="nav-link active" aria-current="page" id="new-location-nav" to="/manufacturers/new">
              New Manufacturer
            </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/models/">Car Models</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/models/new/">Create Models</NavLink>
            </li>
            <li className="nav-item">
            <NavLink className="nav-link active" aria-current="page" id="automobile-nav" to="/automobiles">
              Automobiles
            </NavLink>
            </li>
            <li className="nav-item">
            <NavLink className="nav-link active" aria-current="page" id="new-automobile-nav" to="/automobiles/new">
              New Automobile
            </NavLink>
            </li>
            <li className="nav-item">
            <NavLink className="nav-link active" aria-current="page" id="new-technicians-nav" to="/technicians">
              Add Technicians
            </NavLink>
            </li>
            <li className="nav-item">
            <NavLink className="nav-link active" aria-current="page" id="appointmentlist" to="/appointments">
              Appointments
            </NavLink>
            </li>
            <li className="nav-item">
            <NavLink className="nav-link active" aria-current="page" id="new-appointment-nav" to="/appointments/new">
              New Appointment
            </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/salesperson/">Add New Salesperson</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/customer/">Add New Customer</NavLink>
            </li>
            <li className='nav-item'>
            <NavLink className="nav-link active" aria-current="page" to="/sales/">Create Sale</NavLink>
            </li>
            <li className="nav-item">
            <NavLink className="nav-link active" aria-current="page" id="appointmenthistory" to="/appointments/history">
              Appointment History
            </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" id="new-appointment-history-nav" to="/sales/history">List of Sales</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" id="sale-history" to="/sales/history/hx">Sales Person History</NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
