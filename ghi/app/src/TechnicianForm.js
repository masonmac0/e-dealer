import React, {useState} from 'react';

function TechnicianForm(){

    const [name, setName]= useState('');
    const [employeeNumber, setEmployeeNumber] = useState('');

    const handleNameChange = event => {
        const value = event.target.value
        setName(value)
    }

    const handleEmployeeNumber = event => {
        const value = event.target.value
        setEmployeeNumber(value)
    }
    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {}
        data.name = name;
        data.employee_number = employeeNumber
        const technicianUrl = `http://localhost:8080/api/technicians/`
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }
        const response = await fetch(technicianUrl, fetchConfig);
        if (response.ok) {
            const newTechnician = await response.json();
            setName('')
            setEmployeeNumber('')
        }

    }
    return (
        <div className="container">
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a new Technician</h1>
                <form onSubmit={handleSubmit} id="create-technician-form">
                <div className="form-floating mb-3">
                    <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control"></input>
                    <label> Name </label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleEmployeeNumber} value={employeeNumber} placeholder="Emoployee Number" required type="text"  id="employee_number" className="form-control"></input>
                    <label> Employee Number </label>
                </div>
                <button className="btn btn-primary">Create</button>
                </form>
            </div>
            </div>
        </div>
        </div>
        );
    }
export default TechnicianForm
