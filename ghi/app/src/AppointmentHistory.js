import { useState, useEffect } from 'react'

function AppointmentHistory() {
    const [appointments, setAppointments] = useState([]);
    const [sortVIN, setSearchTerm] = useState('');

    const getAppointments = async () => {
        const appointmentsUrl = 'http://localhost:8080/api/appointments/'
        const appointmentsResponse = await fetch(appointmentsUrl);

        if (appointmentsResponse.ok){
            const data = await appointmentsResponse.json()
            const appointment = data.appointments
            setAppointments(appointment)
            }
        }

        const handleSortByVinChange = event => {
            setSearchTerm(event.target.value);
            };

        const filteredAppointments = appointments.filter(appointment =>
            appointment.vin.includes(sortVIN)
            );

    useEffect(() => {
        getAppointments();
        }, []);

return (
        <>
            <div className="mb-3">
                <h3> Sort by VIN</h3>
                <input type="text" placeholder="Enter VIN here" value={sortVIN} onChange={handleSortByVinChange}/>

        <div className="container">
                <table className="table table-striped">
                <thead>
                    <tr>
                    <th>VIN</th>
                    <th>Customer Name</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Technician</th>
                    <th>VIP</th>
                    <th>Reason</th>
                    <th>Progress</th>
                    </tr>
                </thead>
            <tbody>
        {filteredAppointments.map((appointment) => (
            <tr key={appointment.id}>
            <td>{appointment.vin}</td>
            <td>{appointment.customer_name}</td>
            <td>{appointment.date}</td>
            <td>{appointment.time}</td>
            <td>{appointment.technician?.name}</td>
            <td>{appointment.vip ? "Yes" : "No"}</td>
            <td>{appointment.reason}</td>
            <td>{appointment.completed ? "Complete" : "In Progress"}</td>
            </tr>
            ))}
        </tbody>
        </table>
        </div>
    </div>
    </>
    );
    }

export default AppointmentHistory;
