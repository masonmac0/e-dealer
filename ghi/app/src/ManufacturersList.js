import {useState, useEffect} from 'react'



function ManufacturerList() {
    const[manufacturers, setManufacturers] = useState([])
    const getManufacturers = async () => {
        const manufacturerUrl = 'http://localhost:8100/api/manufacturers/'
        const manufacturerResponse = await fetch(manufacturerUrl);
        if (manufacturerResponse.ok){
            const data = await manufacturerResponse.json()
            const manufacturer = data.manufacturers
            setManufacturers(manufacturer)
            }
        }

    useEffect( () => {getManufacturers()}, [])



    if (manufacturers === undefined) {
        return null;
        }


    return (
        <>
        <div className="container">
            <table className="table table-striped">
            <thead>
                <tr>
                <th>Name</th>
                <th>id</th>
                </tr>
                </thead>
                <tbody>
                {manufacturers.map((manufacturer) => {
                    return (
                    <tr key={manufacturer.id}>
                    <td>{manufacturer.name}</td>
                    <td>{manufacturer.id}</td>
                    </tr>
                );
                })}
                </tbody>
            </table>
            </div>
        </>
        );
    }




export default ManufacturerList;
