import { useState, useEffect } from 'react'

function SalesHistory() {
    const [sale, setSales] = useState([]);
    const [employee, setEmployee]= useState([]);


    const getSales = async () => {
        const salesUrl = 'http://localhost:8090/api/sales/'
        const salesResponse = await fetch(salesUrl);

        if (salesResponse.ok){
            const data = await salesResponse.json()
            const sale = data.sale
            setSales(sale)
            }
        }

    const getEmployee = async () => {
        const employeeUrl = 'http://localhost:8090/api/salesperson/'
        const employeeResponse = await fetch(employeeUrl);

        if (employeeResponse.ok){
            const data = await employeeResponse.json()
            const employee = data.sales_person
            setEmployee(employee)
            }
        }
    const[employees, SetEmployees] = useState('')

        const handleEmployeesChange = (event) => {
            const value = event.target.value;
            SetEmployees(value)
        }

        const filteredSales = sale.filter(
            (sale) => sale.sales_person.name === employees
        );

    useEffect(() => {
        getSales(); getEmployee();
        }, []);

return (
        <>
        <div>
            <div className="mb-3">
                <select onChange={handleEmployeesChange} value={employees} required id="employee_name"  name="employee_name" className="form-select">
                    <option value="" >Select an Employee</option>
                    {employee.map(employee =>{
                        return (
                            <option value={employee.name} key={employee.id} >
                            {employee.name}
                            </option>
                        );
                    })}
                </select>
            </div>
        <div className="container">
                <table className="table table-striped">
                <thead>
                    <tr>
                    <th>Sales Person</th>
                    <th>Customer Name</th>
                    <th>VIN</th>
                    <th>Price</th>
                    </tr>
                </thead>
            <tbody>

        {filteredSales.map((sale) => (
            <tr key={sale.id}>
            <td>{sale.sales_person.name}</td>
            <td>{sale.customer.name}</td>
            <td>{sale.automobile.vin}</td>
            <td>{sale.sale_price}</td>
            </tr>
            ))}
        </tbody>
        </table>
        </div>
    </div>
    </>
    );
    }

export default SalesHistory;
