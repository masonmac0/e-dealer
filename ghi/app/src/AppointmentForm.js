import React, {useState, useEffect} from 'react';

function AppointmentForm(){
    const [autos, setAutomobiles] = useState([]);
    const [technicians, setTechnicians] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/automobiles/';


        const response = await fetch(url);



        if (response.ok){
            const data = await response.json();
            setAutomobiles(data.autos)

            }

        };
    const fetchData2 = async () => {
        const url2 = 'http://localhost:8080/api/technicians/';

        const response2 = await fetch(url2);



        if (response2.ok){
            const data2 = await response2.json();
            setTechnicians(data2.technicians)
            }
        };


    const [date, setDate]= useState('');
    const [time, setTime] = useState('');
    const [customerName, setCustomerName] = useState('');
    const [reason, setReason] = useState('');
    const [technician, setTechnician] = useState('');
    const [vin, setVIN] = useState('');

    const handleDateChange = event => {
        const value = event.target.value
        setDate(value)
    }

    const handleTimeChange = event => {
        const value = event.target.value
        setTime(value)
    }
    const handleCustomerNameChange = event => {
        const value = event.target.value
        setCustomerName(value)
    }
    const handleReasonChange = event => {
        const value = event.target.value
        setReason(value)
    }

    const handleTechnicianChange = event => {
        const value = event.target.value
        setTechnician(value)
    }
    const handleVIN = event => {
        const value = event.target.value
        setVIN(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {}
        data.date = date;
        data.time= time
        data.customer_name = customerName
        data.reason = reason

        data.technician = technician
        data.vin= vin



        const appointmentUrl = `http://localhost:8080/api/appointments/`
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }
        const response = await fetch(appointmentUrl, fetchConfig);
        if (response.ok) {
            const newAppointment = await response.json();
            setDate('')
            setTime('')
            setCustomerName('')
            setReason('')
            setTechnician('')
            setVIN('')
        }

    }
    useEffect(() => {
        fetchData2();
        }, []);


    return (
        <div className="container">
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a new Appointment</h1>
                <form onSubmit={handleSubmit} id="create-appointment-form">
                <div className="form-floating mb-3">
                    <input onChange={handleDateChange} value={date} placeholder="Date" required type="date" name="date" id="date" className="form-control"></input>
                    <label> Date </label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleTimeChange} value={time} placeholder="Time" required type="time"  id="time" className="form-control"></input>
                    <label> Time </label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleCustomerNameChange} value={customerName} placeholder="Customer Name" required type="text"  id="customer_name" className="form-control"></input>
                    <label> Customer Name</label>
                </div>
                <div className="mb-3">
                    <label> Reason</label>
                    <textarea onChange={handleReasonChange} value={reason} placeholder="Reason" required type="text"  id="reason" className="form-control" rows="2"></textarea>
                </div>
                <div className="form-floating mb-3">
                    <select onChange={handleTechnicianChange} value={technician} placeholder="Technician" type="number"  id="technician" className="form-control">
                    <option value ="" > Technician Name</option>
                    {technicians.map(tech =>{
                        return (
                            <option value={tech.id} key={tech.id} >
                                {tech.name}
                            </option>
                        )
                    })}
                    </select>
                </div>
                <div className="mb-3">
                    <label> VIN</label>
                    <textarea onChange={handleVIN} value={vin} placeholder="VIN" required type="text"  id="vin" className="form-control"></textarea>
                </div>
                <button className="btn btn-primary">Create</button>
                </form>
            </div>
            </div>
        </div>
        </div>
        );
    }
export default AppointmentForm
