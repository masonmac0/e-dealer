import {useState, useEffect} from 'react'


function AutomobileList() {
    const[autos, setAutomobiles] = useState([])
    const fetchData = async () => {
        const url = 'http://localhost:8100/api/automobiles/';


        const response = await fetch(url);



        if (response.ok){
            const data = await response.json();
            setAutomobiles(data.autos)

            }

        };

    useEffect( () => {fetchData();}, [])




    if (autos === undefined) {
        return null;
        }
    return (
        <>
        <div className="container">
            <table className="table table-striped">
            <thead>
                <tr>
                <th>VIN</th>
                <th>Color</th>
                <th>Year</th>
                <th>Model</th>
                <th>Manufacturer</th>
                </tr>
                </thead>
                <tbody>
                    {autos.map((auto) => {
                        return (
                        <tr key={auto.id}>
                        <td>{auto.vin}</td>
                        <td>{auto.color}</td>
                        <td>{auto.year}</td>
                        <td>{auto.model.name}</td>
                        <td>{auto.model.manufacturer.name}</td>
                        </tr>
                    );
                    })}
                </tbody>
            </table>
            </div>
        </>
        );
    }




export default AutomobileList;
