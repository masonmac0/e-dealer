import React, {useState} from "react";

function NewCustomerForm() {
    const [name, setName] = useState("");
    const [address, setAddress] = useState("");
    const [phone_number, setPhoneNumber] = useState("");

    const handleNameChange = (e) => {
        const value = e.target.value;
        setName(value);
    }
    const handleAddressChange = (e) => {
        const value = e.target.value;
        setAddress(value);
    }
    const handlePhoneNumberChange = (e) => {
        const value = e.target.value;
        setPhoneNumber(value);
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        const data = {};
        data.name = name;
        data.address = address;
        data.phone_number = phone_number;


        const createCustomerUrl = 'http://localhost:8090/api/customer/';
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        };
        const response = await fetch(createCustomerUrl, fetchConfig);
        if (response.ok) {
            const newCustomer = await response.json();
            setName("");
            setAddress("");
            setPhoneNumber("");
        }
    }

    return (
        <div className="my-5 container">
        <div className="row">
        <div className="col">
          <div className="card shadow">
            <div className="card-body">
                <form onSubmit={handleSubmit} id="customer-form">
                <h1 className="card-title">New Customer</h1>
                <p className="mb-3">
                Please fill out this form to create a new customer.
                </p>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleNameChange} required placeholder="name" type="text" id="name" name="name" className="form-control" value={name}/>
                      <label htmlFor="name">Customer Name</label>
                  </div>
                  <div className="col">
                  <div className="form-floating mb-3">
                    <input onChange={handleAddressChange} required placeholder="Address" type="text" id="address" name="address" className="form-control" value={address}/>
                    <label htmlFor="street">Street</label>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                        <input onChange={handlePhoneNumberChange} required placeholder="Phone Number" type="text" id="phone_number" name="phone_number" className="form-control" value={phone_number}/>
                        <label htmlFor="Phone Number">Phone Number</label>
                    </div>
                  </div>
                  <div className="col">
                    <button type="submit" className="btn btn-primary">Submit</button>
                  </div>
            </div>
            </div>
            </form>
            </div>
          </div>
        </div>
        </div>
        </div>
    );


}

export default NewCustomerForm;
