# CarCar

Team:

* Person 1 - Mason McInerny: Service
* Person 2 - Kalani Hines: Sales

## Design
E-Dealer is a web applicaion designed to track an auto dealership by tracking inventory of vehicles, sales and maintenance. Each application is split into itsown microservice, utilizing RESTful API's to provide information within the application. This project utilizes Docker, Insomnia, and PostreSQL.

### Features
E-Dealer Web Application Features:

Manage Auto Manufacturers, Vehicle Models, and Vehicles. Including Creating and Updating.
Create Salespeople, Technicians and Potential Customers
Track Sales and Records of Salespeople
Manage Technicians and Service Appointments
View Service Appointment List with VIP feature for inventory vehicles
Search Service History by VIN.

#### Install the Application
How to begin the project:

Fork repository from: https://gitlab.com/KalaniHines/e-dealer
Clone the project in your terminal using: git clone { project slug here }

Run these three commands in the terminal

docker volume create beta-data
docker-compose build
docker-compose up
Navigate to http://localhost:3000/


the port for the inventory microservice is http://localhost:8100/

Step by Step instructions to run the project

This project is designed in Three parts. The inventory, sales and also the service.

To run the project it begins in the Inventory. In the inventory for the app to function it will need to have a maunfacturer created, This is essentially the first step as the rest of the project is dependendent on the manufacturer.

The manufacturer contains a name field that has to be unique with a max
length of 100 characters. The reason this has to be unique is due to that there should be only be one of each manufacturer as to avoid confusion when creating a vehicle model.

The vehicle model is the second step.
A vehicle model contains a name of 100 characters and a picture URL.
This model is depended on a manufacturer being created first to allow for it to be possible to have a model added.

The third portion is creating the Automoble. The Automobile is dependent on a model being created. It also as fields for color of the car and also the year. The most important aspect of the automobile model is the vin. The vin has to be unique as each car will not have the same one so its used for tracking throughout both the service and sales microservice

The HTTP urls are:


Manufacturers = http://localhost:8100/api/manufacturers/
individual manufacturers =http://localhost:8100/api/manufacturers/id/



Models = http://localhost:8100/api/models/
individual models = http://localhost:8100/api/models/id/




Automobiles = http://localhost:8100/api/automobiles/
individual Automobiles  = http://localhost:8100/api/automobiles/:vin/



##### Sales microservice

Models for Sales MS:

AutomobileVO
Salesperson
PotentialCustomer
Sales (>>> Sales has foreign keys related to AutoVO, Salesperson and PotentialCustomer <<<)

Sales Integration with the Inventory Microservice:

The Sales microservice will retrieve data from the Automobiles Inventory microservice and map it to an AutomobileVO model (value object). The AutomobileVO model will have an additional BooleanField called 'sold', which is set to 'false' by default. When a vehicle is sold using the Sales form, the 'sold' value for that vehicle will be updated to 'true'. This will ensure that sold cars are filtered out from the VIN dropdown on the Sales form. Additionally, the Salesperson record and the list of all sales will be updated in real-time to reflect the sale of the vehicle.

Insomnia URL paths and JSON bodies:

POST(create) Salesperson:localhost:8090/api/salesperson/
JSON body:
{
	"name": "Mike",
	"employee_number": 1
}
GET(list) Salesperson: localhost:8090/api/salesperson/

POST(create) Customer: localhost:8090/api/customer/
JSON body:
{
			"name": "Chad",
			"street": "123 Oak",
			"city": "Folsom",
			"zip_code": "95662",
			"state": "Ca",
			"phone": "6501234555"
	}
GET(list) Customer: localhost:8090/api/customer/

POST(create) Sale: localhost:8090/api/sales/
JSON body:
{
  "sale_price": "22000",
  "vin": "1235465799900",
	"customer": "Chad",
	"salesperson": "Mike"
}
GET(list)sale: localhost:8090/api/sales/

GET(list) Salesperson history: localhost:8090/api/sales/<:id>/






###### Service microservice

Explain your models and integration with the inventory
microservice, here.

The port that the service model uses is: http://localhost:8080/

The models I have are: AutomobileVO, Tech (aka technician), and ServiceAppointment,

The value object is AutomobileVO. This is the value object because it is it same data that is used in the creation of an Automobile in the inventory  microservice.

The Automobile uses the poller to grab the details of an Automobile object from the Inventory API once every minute. So once an Automobile is created the AutomobileVO will have the same field as the Automobile model (color, year, and the vin). I also included import_href to allow for checking of an automobileVO object but I ended up not using this feature. This is the integration i have with the with inventory as that if the poller has no data there is no automobiles in the inventory that would require service.

The second model I created is the tech model. This is to allow me to create a technician. I added a name field to name each technician and also employee number. The name allows for implementation later on when creating an appointment to have better readability. The number is used to allow each employee to have a unique number to allow signing up an appointment to an individual employee without it being assiged to two employees.

The ServiceAppointment Model has a date and time field to allow for scheduling, a customer name field, the reason for the appointment, if they are a vip or not, the assigned technician and finally the vin.
The customer name does need to be unique as this model is dependent on the vin number.  The Technician field has a foreignKey on a technician to be created because it uses the technician id to create an appointmen and an appointment cannot be created unless it is assigned to a technician This model also utilizes the class method to check if the vin exist within the AutomobileVO objects and if it does exist within the AutomobileVO it will change the vip field (which is defaulted to false) to be set to true.

The tech model and the and the service appointment model both have there own CRUD route but both are on the same port of http://localhost:8080/api/.

I created the http for tech as: http://localhost:8080/api/technicians/
This HTTP url allows for a GET request and also a POST request. I only allowed these two request as each employee number is unique and if someone tries to change the number of an employee it will not be allowed.

json body of technician:

{
	"name": "insert name",
	"employee_number": 1
}

The http for ServiceAppointments is: http://localhost:8080/api/appointments/. This alows for a GET and a POST request. This allows for appointments to be seen in a list version and to create new appointments

json body of appointments:

 the technician value is the technician id which can be fouund at a get request of http://localhost:8080/api/technicians/

{
    "date": "2023-03-15",
    "time": "09:00:00",
    "customer_name": "insert name ",
    "reason": "insert reason",
    "technician": 1,
    "vin": "insert vin here"
}

vin can be an automobile created through create automobile in inventory or a new vin number

For changes on individual Appoinment I created the HTTP http://localhost:8080/api/appointments/ID/. This url allows a get request to allow for individual appointment to be seen, a put request to update appointments and also a delete request. This update and delete are there so in the front end the appointment can get canceled and marked as complete in order to mark an appointment as complete.

###### Inventory Insomnia
manufacturer: GET localhost:8100/api/manufacturers/
manufacturer: POST localhost:8100/api/manufacturers/
JSON body:
{
	"name": "Chevy"
}

models: GET localhost:8100/api/models/
models: POST localhost:8100/api/models/
JSON body:
{
  "name": "LX",
  "picture_url": "",
  "manufacturer_id": 1
}

automobiles: GET localhost:8100/api/automobiles/
automobiles: POST localhost:8100/api/automobiles/
JSON body:
{
  "color": "Blue",
  "year": 2016,
  "vin": "1235465799900",
  "model_id": 1
}


##### The diagram is located at the file diagram-1.png
##### The diagram is located at the file diagram-1.png
##### The diagram is located at the file diagram-1.png
##### The diagram is located at the file diagram-1.png
