import django
import os
import sys
import time
import json
import requests


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "service_project.settings")
django.setup()

# Import models from service_rest, here.
# from service_rest.models import Something

from service_rest.models import AutomobileVO



def get_automobiles():
    url = "http://inventory-api:8000/api/automobiles/"
    response = requests.get(url)
    content = json.loads(response.content)
    print(content)
    for automobile in content["autos"]:
        vin = automobile["vin"]
        try:
            AutomobileVO.objects.update_or_create(
                import_href = automobile["href"],
                vin = vin,
                defaults={
                    "vin": vin
                },
            )
        except Exception as e:
            print("failed to insert", vin, e)

def poll():
    while True:
        print('Sales poller polling for data')
        try:
            get_automobiles()
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
