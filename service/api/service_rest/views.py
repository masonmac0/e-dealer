from django.shortcuts import render
from django.http import JsonResponse
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
import json
from .models import Tech, AutomobileVO, ServiceAppointment

class TechEncoder(ModelEncoder):
    model = Tech
    properties = [
        "name",
        "employee_number",
        "id",
    ]

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "id",
        "import_href",
        "color",
        "year",
        "vin",
    ]

class ServiceAppointmentList(ModelEncoder):
    model = ServiceAppointment
    properties = [
        "id",
        "date",
        "time",
        "customer_name",
        "reason",
        "completed",
        "vip",
        "vin",
        'technician',
    ]
    encoders = {
        'technician' : TechEncoder(),
    }
class ServiceAppointmentDetail(ModelEncoder):
    model = ServiceAppointment
    properties = [
        "id",
        "date",
        "time",
        "customer_name",
        "reason",
        "completed",
        "vip",
        "vin",
        "technician"
    ]
    encoders = {
        'technician' : TechEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_service_appointments(request):
    if request.method == "GET":
        appointments = ServiceAppointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder= ServiceAppointmentList,
        )
    else:
        try:
            content = json.loads(request.body)

            technician = Tech.objects.get(pk=content["technician"])
            content["technician"] = technician

            appointment = ServiceAppointment.create(**content)

            return JsonResponse(
                appointment,
                encoder= ServiceAppointmentDetail,
                safe = False,
            )
        except:
            response = JsonResponse(
                {"message": "could not create appointment"}
                )
            response.status_code = 400
            return response

@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_appointments(request,pk):
    if request.method == "GET":
        appointment = ServiceAppointment.objects.get(id=pk)
        return JsonResponse(
            appointment,
            encoder=ServiceAppointmentDetail,
            safe=False,
        )
    if request.method == "DELETE":
        count, _ = ServiceAppointment.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        ServiceAppointment.objects.filter(id=pk).update(**content)
        appointment = ServiceAppointment.objects.get(id=pk)
        return JsonResponse(
            appointment,
            encoder=ServiceAppointmentDetail,
            safe=False
        )

@require_http_methods(["GET", "POST"])
def api_technician(request):
    if request.method == "GET":
        technicians = Tech.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder= TechEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            tech = Tech.objects.create(**content)
            return JsonResponse(
                tech,
                encoder= TechEncoder,
                safe = False,
            )
        except:
            response = JsonResponse(
                {"message": "Error adding new Technician"}
            )
            response.status_code
            return response
