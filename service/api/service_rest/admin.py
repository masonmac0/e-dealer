from django.contrib import admin

# Register your models here.
from .models import AutomobileVO, Tech, ServiceAppointment

@admin.register(AutomobileVO)
class AutoMobileVO(admin.ModelAdmin):
    pass

@admin.register(Tech)
class Tech(admin.ModelAdmin):
    pass

@admin.register(ServiceAppointment)
class ServiceAppointment(admin.ModelAdmin):
    pass
